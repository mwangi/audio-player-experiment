// JavaScript Document
module.exports = {
	options: {
		compatibility: 'ie8',
		keepSpecialComments: '*',
		noAdvanced: true
	},
	minify:
	{
		src: 'dist/css/audioplayer.css',
		dest: 'dist/css/audioplayer.min.css'
	}
};